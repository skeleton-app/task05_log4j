package com.epam;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class QuartzWatch extends Watch implements IQuartzable {
    Date date;
    Calendar calendar;
    SimpleDateFormat simpleDateFormat,simpleTimeFormat;

    QuartzWatch(){
        simpleDateFormat = new SimpleDateFormat("dd/mm/yyyy");
        simpleTimeFormat = new SimpleDateFormat("hh:mm");
    }

    @Override
    protected String getTime() {
        date = new Date();
        calendar = Calendar.getInstance();
        calendar.setTime(date);
        //noinspection deprecation
        Integer timeHours = date.getHours();
        //noinspection deprecation
        Integer timeMinutes = date.getMinutes();
        //noinspection deprecation
        Integer timeSeconds = date.getSeconds();
        return timeHours+":"+timeMinutes+":"+(timeSeconds*IQuartzable.MISTAKES_VALUE)/100;
    }
    /** @noinspection deprecation*/
    @Override
    protected void setTime(Integer hour, Integer minute, Integer second) {
        date = new Date();
        date.setHours(hour);
        date.setMinutes(minute);
        date.setSeconds(second);
    }
}
