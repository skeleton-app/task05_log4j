package com.epam.SMSSender;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    public static final String ACCOUNT_SID = "AC16cdd42b1e4b4207bcfb00d62de9085c";
    public static final String AUTH_TOKEN = "2ce3e6925c625c14cf966a991ef62993";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(new PhoneNumber("+380687814917"),
                new PhoneNumber("+19147126420"), str) .create();
    }
}
