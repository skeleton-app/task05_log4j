package com.epam;
import javax.swing.text.DateFormatter;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static java.lang.String.format;

public class MechanicalWatch extends Watch implements IMechanical{
    Date date;
    Calendar calendar;
    SimpleDateFormat simpleDateFormat;

    public MechanicalWatch(){
    }
    @Override
    protected String getTime() {
        date = new Date();
        calendar = Calendar.getInstance();
        calendar.setTime(date);
        //noinspection deprecation
        Integer timeHours = date.getHours();
        //noinspection deprecation
        Integer timeMinutes = date.getMinutes();
        //noinspection deprecation
        Integer timeSeconds = date.getSeconds();
        return timeHours+":"+timeMinutes+":"+(timeSeconds*IMechanical.MISTAKES_VALUE)/100;
    }

    /** @noinspection deprecation*/
    @Override
    protected void setTime(Integer hour, Integer minute, Integer second) {
        date = new Date();
        date.setHours(hour);
        date.setMinutes(minute);
        date.setSeconds(second);
    }

}
