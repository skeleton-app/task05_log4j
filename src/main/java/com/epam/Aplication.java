package com.epam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Aplication {
    private static Logger logger = LogManager.getLogger(Aplication.class);

    public static void main(String[] args) {

        Watch RolexDaytona = new QuartzWatch();
        System.out.println(RolexDaytona.getTime());
        Watch LuevVutonne = new MechanicalWatch();
        System.out.println(LuevVutonne.getTime());

        logger.error("Error...");
        logger.debug("Debug...");
        logger.info("Info...");
        logger.trace("Trace...");
        logger.fatal("FATAL!!...");
        logger.warn("Warn...");
    }
}
