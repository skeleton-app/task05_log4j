package com.epam;

public abstract class Watch {
   protected String date;
   protected int hour;
   protected int minute;
   protected int second;

   protected abstract String getTime();
   protected abstract void setTime(Integer hour,Integer minute,Integer second);
}
